'use strict';

const http = require('http-status');
var config;

module.exports.load = (settings) => {
    config = settings
};

module.exports.hello = (req, res, next) => {
    console.log(config);
    res.send(http.OK, {result: "world"});
    return next();
};