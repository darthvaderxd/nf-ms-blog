'use strict';

/* istanbul ignore next */
const port = process.env.PORT || 8080;
const handler = require('./handler');

var server;
var config = require('../config')();

module.exports.run = (options, callback) => {
    server = require('./server')(options);
    handler.load(config);

    server.listen(port, () => {
        /* istanbul ignore if */
        if (!process.env.SILENT) {
            console.log('%s listening at %s', server.name, server.url);
        }

        /* istanbul ignore if */
        if (typeof callback != 'undefined') {
            callback();
        }
    });

    /**
     * routes here
     */
    server.get('/hello', handler.hello);
};


module.exports.stop = () => {
    server.close();
};